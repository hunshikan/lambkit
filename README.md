### JAVA 分布式快速开发框架 Lambkit

Lambkit是基于JFinal的分布式Javaweb快速开发框架，其核心设计目标是极速开发，快速应用。将项目开发的基本要素集合成企业级开发解决方案，快速搞定项目，为您节约更多时间，去陪恋人、家人和朋友 ;)

Lambkit是在我们项目开发过程中不断学习和积累起来的一个基础开发框架，我们将一直不断完善和更新。

#### Lambkit有如下主要特点
- 集成了多种流行技术：shiro、redis、ehcache、swagger、montan、zbus、TongLinkQ。
- 开发了多种基础功能：mq、rpc、mail、WebSocket、分布式节点、反向代理、代码自动生成、动态表单、动态SQL、多维分析、表格动态管理。
- 实现了多种应用功能：微服务架构、基于zbus或motan的RPC框架、用户与权限管理、基于Redis的分布式session技术、基于zbus的消息框架、E-Mail后台发送技术、后台接口展示技术、分布式节点动态管理、自定义反向代理。
- 配套了多种独立系统：UPMS单点登录系统（来源于项目[zheng](https://gitee.com/shuzheng/zheng)）、Mgrdb表格管理系统等，应用于不同的行业。

**Lambkit 极速开发，快速应用，QQ群欢迎你的加入: 276782534**

**Lambkit 官方网站：[http://www.lambkit.com](http://www.lambkit.com)**


